import React from 'react'
import FeaturesPage from './components/FeaturesPage'
import LandingPage from './components/LandingPage'
import MobileHeader from './nav/MobileHeader'
// import SecondComponent from './SecondComponent'
// import ThirdComponent from './ThirdComponent'
// import FourthComponent from './FourthComponent'
// import FifthComponent from './FifthComponent'

export default function App() {
  return (
    <React.Fragment>
      <MobileHeader />
      <div>
        <LandingPage />
        <FeaturesPage />
        <div className='page' style={{ backgroundColor: 'red' }}>
          1
        </div>
        <div className='page' style={{ backgroundColor: 'blue' }}>
          1
        </div>
        <div className='page' style={{ backgroundColor: 'red' }}>
          1
        </div>
      </div>
    </React.Fragment>
  )
}

// TODOS
// - remove scrollbar
// - add progress bar
