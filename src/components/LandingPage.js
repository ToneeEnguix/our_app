/** @jsxRuntime classic */
/** @jsx jsx */
import { jsx } from '@emotion/react'
import facepaint from 'facepaint'

// RESPONSIVENESS SETTINGS
const breakpoints = [700, 990]
const mq = facepaint(breakpoints.map((bp) => `@media (min-width: ${bp}px)`))

export default function LandingPage() {
  return (
    <div className='page' css={pageStyle}>
      <div className='topDesktop'>
        <div className='left'>
          <img
            alt='logo'
            src='https://assets.website-files.com/5432b1b70282e4a60a036921/54340478f80673470f2b5a8d_logo.png'
            className='logo'
          />
          <h2 className='uppercase'>App landing page</h2>
        </div>
        <div className='right'>
          <button className='pointer' style={{ margin: '0 1rem' }}>
            <img
              src='https://assets.website-files.com/5432b1b70282e4a60a036921/5432c2740282e4a60a036aef_ico1.png'
              alt='apple'
            />
            <p className='uppercase'>App Store</p>
          </button>
          <button className='pointer'>
            <img
              src='https://assets.website-files.com/5432b1b70282e4a60a036921/5432c34b50bc361265fc3bf9_ico2.png'
              alt='android'
            />
            <p className='uppercase'>Play Store</p>
          </button>
        </div>
      </div>

      <div className='line' />
      <div className='bottom'>
        <h2>Extraordinary app that makes your everyday life so much easier!</h2>
        <img
          src='https://assets.website-files.com/5432b1b70282e4a60a036921/5e83503afba7fd5258b3fa62_app-1.png'
          alt='phone'
        />
      </div>
    </div>
  )
}

const pageStyle = mq({
  padding: ['5rem 2rem 0', '5rem 6rem 0', '0 10%'],
  color: 'white',
  backgroundImage:
    "url('https://assets.website-files.com/5432b1b70282e4a60a036921/54405bdccdb0d892441a1dfc_background2.jpg')",
  backgroundSize: 'cover',
  '.topDesktop, .left, .right, button': {
    display: 'flex',
    alignItems: 'center',
  },
  '.topDesktop': {
    display: ['none', 'none', 'flex'],
    padding: '3rem 0',
    justifyContent: 'space-between',
  },
  '.left': {
    '.logo': { width: '170px' },
    h2: {
      marginLeft: '1rem',
      padding: '.6rem 1rem',
      fontSize: '.7rem',
      fontWeight: 100,
      backgroundImage: 'linear-gradient(90deg, #3c8dc5, #3bc4b4)',
      letterSpacing: '1px',
    },
  },
  button: {
    color: 'white',
    padding: '.8rem 1rem',
    alignItems: 'center',
    border: '1px solid #8BA6BE',
    transition: 'all .2s linear',
    fontWeight: 100,
    fontSize: '.7rem',
    letterSpacing: '1px',
    ':hover': {
      backgroundColor: '#3C8CC5',
      borderColor: '#3C8CC5',
    },
    img: {
      width: '20px',
      margin: '0 .6rem 0 0',
    },
  },
  '.line': {
    display: ['none', 'none', 'flex'],
    backgroundImage:
      'linear-gradient(90deg, #3c8dc5, #6fb7e9 36%, #3bc4b4 65%, #7ef7a9)',
    margin: '0 auto',
    height: '2px',
  },
  '.bottom': {
    padding: ['3rem 0 0', '5rem 0 0', '5rem 0 0'],
    h2: {
      fontFamily: "'Lato', sans-serif",
      textAlign: 'center',
      fontSize: ['2.4rem', '2.6rem', '3.4rem'],
      fontWeight: 300,
      margin: '0 auto',
      lineHeight: ['3.8rem', '4rem', '5rem'],
      maxWidth: '80vw',
    },
    img: {
      width: ['80%', '80%', '60%'],
      margin: '2rem auto 0',
      display: 'block',
    },
  },
})
