/** @jsxRuntime classic */
/** @jsx jsx */
import { jsx } from '@emotion/react'
import facepaint from 'facepaint'

// RESPONSIVENESS SETTINGS
const breakpoints = [700, 990]
const mq = facepaint(breakpoints.map((bp) => `@media (min-width: ${bp}px)`))

export default function FeaturesPage() {
  return (
    <div className='page' css={pageStyle}>
      <p className='uppercase awesome'>Take a look at our awesome features</p>
      <h2 className='features'>Features you'll love</h2>
      <div className='line' />
      <div className='line' style={{ marginBottom: '2rem' }} />
      <div css={grid}>
        <img
          className='phone desktop'
          src='https://assets.website-files.com/5432b1b70282e4a60a036921/5e83504a0d9871b3b7fc9402_app-2.png'
          alt='phone'
        />
        <div>
          <div className='box'>
            <div className='flex'>
              <div className='logoWrapper'>
                <img
                  src='https://assets.website-files.com/5432b1b70282e4a60a036921/5446e385f3712f7954f33445_ico4.png'
                  alt='customer'
                />
              </div>
              <p>Customer Support</p>
            </div>
            <span>
              A team of professionals is at your disposal to solve any issues
              you might experience
            </span>
          </div>
          <div className='box'>
            <div className='flex'>
              <div className='logoWrapper'>
                <img
                  src='https://assets.website-files.com/5432b1b70282e4a60a036921/5446e33df50742f359ad1795_ico3.png'
                  alt='customer'
                />
              </div>
              <p>Responsive</p>
            </div>
            <span>
              Use the app in any kind of device: mobile, tablet or desktop
            </span>
          </div>
          <div className='box'>
            <div className='flex'>
              <div className='logoWrapper'>
                <img
                  src='https://assets.website-files.com/5432b1b70282e4a60a036921/5446e432c16f2af2595d109d_ico7.png'
                  alt='customer'
                />
              </div>
              <p>Newsletter</p>
            </div>
            <span>
              Receive the latest updates and be up to date with recent trends
              with our weekly mail
            </span>
          </div>
        </div>
        <div className='desktop'>
          <div className='box'>
            <div className='flex'>
              <div className='logoWrapper'>
                <img
                  src='https://assets.website-files.com/5432b1b70282e4a60a036921/5446e408f3712f7954f3344a_ico6.png'
                  alt='customer'
                />
              </div>
              <p>Video Stream</p>
            </div>
            <span>
              Capture your actions in real time and share it with the world!
            </span>
          </div>
          <div className='box'>
            <div className='flex'>
              <div className='logoWrapper'>
                <img
                  src='https://assets.website-files.com/5432b1b70282e4a60a036921/5446e46ec16f2af2595d109e_ico8.png'
                  alt='customer'
                />
              </div>
              <p>Contact Form</p>
            </div>
            <span>
              Get in touch with your audience and receive useful feedback
            </span>
          </div>
          <div className='box'>
            <div className='flex'>
              <div className='logoWrapper'>
                <img
                  src='https://assets.website-files.com/5432b1b70282e4a60a036921/5446e30ff50742f359ad1794_ico2.png'
                  alt='customer'
                />
              </div>
              <p>Made with React</p>
            </div>
            <span>
              Fully customisable website, anything can be reused or recycled
            </span>
          </div>
        </div>
      </div>
    </div>
  )
}

const pageStyle = mq({
  color: 'black',
  textAlign: 'center',
  padding: ['2rem 2rem 0', '2rem 6rem 0', '2rem 10% 0'],
  '.awesome': {
    color: '#3C8CC5',
    fontSize: '.8rem',
    fontWeight: 400,
    fontFamily: '"Open-Sans", sans-serif',
    letterSpacing: '.03rem',
    margin: ['3rem 0 0', '2rem 0 2rem', '1rem 0 1rem'],
  },
  '.features': {
    fontSize: ['2rem', '3rem', '4rem'],
    fontWeight: 300,
    marginBottom: ['2rem', '1rem'],
  },
  '.desktop': {
    display: ['none', 'none', 'block'],
  },
})

const grid = mq({
  display: 'grid',
  gridTemplateColumns: ['1fr', '1fr', '1fr 1fr 1fr'],
  '.phone': {
    margin: '0 auto',
    width: ['50%', '50%', '90%'],
  },
  '.box': {
    display: 'flex',
    flexDirection: 'column',
    // justifyContent: 'center',
    border: '1px solid lightgray',
    padding: ['1.5rem 1.5rem 0', '1.5rem 1.5rem 0', '8% 8% 0'],
    textAlign: 'left !important',
    height: '140px',
    '.flex': { marginBottom: '5%' },
    span: {
      fontSize: '.8rem',
      color: 'gray',
    },
  },
  '.logoWrapper': {
    width: '40px',
    height: '40px',
    borderRadius: '100px',
    backgroundColor: '#3c8dc5',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    padding: '.6rem',
    marginRight: '1rem',
  },
})
