/** @jsxRuntime classic */
/** @jsx jsx */
import { jsx } from '@emotion/react'
import facepaint from 'facepaint'
import { motion, AnimatePresence } from 'framer-motion'
import { useState } from 'react'

// RESPONSIVENESS SETTINGS
const breakpoints = [700, 990]
const mq = facepaint(breakpoints.map((bp) => `@media (min-width: ${bp}px)`))

export default function MobileHeader({ setCurrentPage }) {
  const [isMenuOpen, setIsMenuOpen] = useState(false)

  return (
    <div css={topMobile}>
      <img
        className='logo'
        src='https://assets.website-files.com/5432b1b70282e4a60a036921/54406b2552e2fb242e4e641f_logo-3.png'
        alt='mobile logo blue'
      />
      <div css={menuWrapper}>
        <img
          src='https://img.icons8.com/ios-glyphs/30/000000/menu-rounded.png'
          alt='burger'
          className='pointer burger'
          onClick={() => setIsMenuOpen(!isMenuOpen)}
        />
        <AnimatePresence>
          {isMenuOpen && (
            <motion.div
              className='menu'
              initial={{ opacity: 0 }}
              animate={{ opacity: 1 }}
              exit={{ opacity: 0 }}
            >
              <p onClick={() => setCurrentPage(0)} className='pointer menuItem'>
                Landing
              </p>
              <p onClick={() => setCurrentPage(1)} className='pointer menuItem'>
                About
              </p>
              <p onClick={() => setCurrentPage(2)} className='pointer menuItem'>
                Contact
              </p>
              <p onClick={() => setCurrentPage(3)} className='pointer menuItem'>
                Settings
              </p>
            </motion.div>
          )}
        </AnimatePresence>
      </div>
    </div>
  )
}

const topMobile = mq({
  color: 'white',
  zIndex: 1000,
  display: ['flex', 'flex', 'none'],
  height: '80px',
  backgroundColor: 'white',
  width: '100vw',
  position: 'fixed',
  top: 0,
  left: 0,
  alignItems: 'center',
  justifyContent: 'space-between',
  padding: '0 5rem',
  '.logo': { width: '170px' },
  '.burger': {
    width: '30px',
    filter:
      'invert(36%) sepia(90%) saturate(707%) hue-rotate(169deg) brightness(92%) contrast(87%)',
  },
})

const menuWrapper = mq({
  position: 'relative',
  '.menu': {
    // transition: 'all 400ms ease-in-out',
    position: 'absolute',
    top: '57px',
    right: '-80px',
    '.menuItem': {
      width: '100vw',
      backgroundColor: '#3C8CC5',
      textAlign: 'center',
      lineHeight: '4rem',
      fontWeight: 500,
      fontFamily: '"Roboto", sans-serif',
      ':hover': {
        backgroundColor: '#29658E',
      },
    },
  },
})
