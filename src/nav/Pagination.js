/** @jsxRuntime classic */
/** @jsx jsx */
import { jsx } from '@emotion/react'
import facepaint from 'facepaint'

// RESPONSIVENESS SETTINGS
const breakpoints = [700]
const mq = facepaint(breakpoints.map((bp) => `@media (min-width: ${bp}px)`))

export default function Pagination({ currentPage, setCurrentPage }) {
  const handlePageChange = (num) => {
    setCurrentPage(num)
  }

  const getPagesNumbers = () => {
    const length = 5
    const pageNumbers = Array.from({ length }, (_, i) => i + 1)

    return pageNumbers.map((i) => (
      <div
        key={i}
        onClick={() => handlePageChange(i - 1)}
        style={{
          backgroundColor: currentPage === i - 1 && '#3C8CC5',
          color: currentPage === i - 1 && 'white',
          width: currentPage === i - 1 && '8px',
          height: currentPage === i - 1 && '8px',
        }}
      />
    ))
  }
  return <div css={pageStyle}>{getPagesNumbers()}</div>
}

const pageStyle = mq({
  margin: '0',
  position: 'fixed',
  top: '20px',
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  flexDirection: 'column',
  height: '100%',
  paddingLeft: '1rem',
  div: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'whitesmoke',
    color: 'black',
    borderRadius: '100px',
    width: '10px',
    height: '10px',
    margin: ['0.5rem 0', '1rem 0'],
    cursor: 'pointer',
    border: 'none',
    opacity: '.9',
    transition: 'all .4s linear',
    ':hover': {
      backgroundColor: 'antiquewhite',
    },
  },
})
